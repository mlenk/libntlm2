/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include <ntlm2.h>

#include <stdio.h>		/* printf */
#include <string.h>		/* memcmp */
#include <stdbool.h>		/* bool */

static int
hashtest (const char *passwd, const char *expected, bool ntlm_p)
{
  char out[NTLM2_HASH_LEN];
  int rc;
  size_t i;

  if (ntlm_p)
    rc = ntlm2_ntlmhash (passwd, out);
  else
    rc = ntlm2_lmhash (passwd, out);
  if (rc != NTLM2_OK)
    {
      printf ("hash returned %d\n", rc);
      return 1;
    }

  if (memcmp (out, expected, NTLM2_HASH_LEN) != 0)
    {
      printf ("hash(\"%s\")\n     got: ", passwd);
      for (i = 0; i < NTLM2_HASH_LEN; i++)
	printf ("%02x ", out[i] & 0xFF);
      printf ("\nexpected: ");
      for (i = 0; i < NTLM2_HASH_LEN; i++)
	printf ("%02x ", expected[i] & 0xFF);
      printf ("\nfailure!\n");
      return 1;
    }

  return 0;
}

int
main (void)
{
  return !(hashtest ("SecREt01", "\xff\x37\x50\xbc\xc2\xb2\x24\x12"
		     "\xc2\x26\x5b\x23\x73\x4e\x0d\xac", false) == 0 &&
	   hashtest ("mypasswd", "\x74\xac\x99\xca\x40\xde\xd4\x20"
		     "\x4a\x3b\x10\x8f\x3f\xa6\xcb\x6d", false) == 0 &&
	   hashtest ("test1234", "\x62\x4a\xac\x41\x37\x95\xcd\xc1"
		     "\xff\x17\x36\x5f\xaf\x1f\xfe\x89", false) == 0 &&
	   hashtest ("SecREt01", "\xcd\x06\xca\x7c\x7e\x10\xc9\x9b"
		     "\x1d\x33\xb7\x48\x5a\x2e\xd8\x08", true) == 0 &&
	   hashtest ("mypasswd", "\xf6\x71\x04\x3b\xa0\x8e\x88\x50"
		     "\x0d\x2e\xb5\x27\x9a\xc6\x5e\x53", true) == 0 &&
	   hashtest ("test1234", "\x3b\x1b\x47\xe4\x2e\x04\x63\x27"
		     "\x6e\x3d\xed\x6c\xef\x34\x9f\x93", true) == 0);
}
