/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include "md4.h"

#include <stdio.h>		/* printf */
#include <string.h>		/* memcmp */

static int
md4test (unsigned char *d, size_t l, unsigned char *expected)
{
  unsigned char tmp[_NTLM2_MD4_DIGEST_LENGTH];

  _ntlm2_md4 ((unsigned char *) d, l, tmp);

  if (memcmp (tmp, expected, _NTLM2_MD4_DIGEST_LENGTH) != 0)
    {
      size_t i;

      printf ("MD4(0x");
      for (i = 0; i < l; i++)
	printf ("%02x", d[i] & 0xFF);
      printf (")\n    got   ");
      for (i = 0; i < _NTLM2_MD4_DIGEST_LENGTH; i++)
	printf ("%02x", tmp[i] & 0xFF);
      printf ("\n expected ");
      for (i = 0; i < _NTLM2_MD4_DIGEST_LENGTH; i++)
	printf ("%02x", expected[i] & 0xFF);
      printf ("\n");
      return 1;
    }

  return 0;
}

int
main (void)
{
  return !(md4test ("", 0, "\x31\xd6\xcf\xe0\xd1\x6a\xe9\x31"
		    "\xb7\x3c\x59\xd7\xe0\xc0\x89\xc0") == 0 &&
	   md4test ("a", 1, "\xbd\xe5\x2c\xb3\x1d\xe3\x3e\x46"
		    "\x24\x5e\x05\xfb\xdb\xd6\xfb\x24") == 0 &&
	   md4test ("abc", 3, "\xa4\x48\x01\x7a\xaf\x21\xd8\x52"
		    "\x5f\xc1\x0a\xe8\x7a\xa6\x72\x9d") == 0 &&
	   md4test ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnop"
		    "qrstuvwxyz0123456789", 62,
		    "\x04\x3f\x85\x82\xf2\x41\xdb\x35"
		    "\x1c\xe6\x27\xe1\x53\xe7\xf0\xe4") == 0);
}
