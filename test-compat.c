/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include <ntlm.h>
#include <ntlm2.h>

#include <string.h>		/* memcpy */
#include <stdlib.h>		/* abort */

/* Libntlm implementation */

void
libntlm_init (void)
{
}

void
libntlm_done (void)
{
}

void
libntlm_type1 (const char *domain, const char *workstation,
	       char **out, size_t * len)
{
  tSmbNtlmAuthRequest request;
  char *p;

  buildSmbNtlmAuthRequest (&request, domain, workstation);
  p = (void *) &request;

  *len = SmbLength (&request);
  *out = malloc (*len);
  if (*out == NULL)
    abort ();
  memcpy (*out, p, *len);
}

void
libntlm_type3 (const char *type2, size_t type2len,
	       const char *username, const char *password,
	       char **out, size_t * len)
{
  tSmbNtlmAuthChallenge challenge;
  tSmbNtlmAuthResponse response;
  char *p;

  memcpy (&challenge, type2, type2len);

  buildSmbNtlmAuthResponse (&challenge, &response, username, password);
  p = (void *) &response;

  *len = SmbLength (&response);
  *out = malloc (*len);
  if (*out == NULL)
    abort ();
  memcpy (*out, p, *len);
}

/* Libntlm2 implementation */

ntlm2c_t ctx;

void
libntlm2_init (void)
{
  int rc;

  rc = ntlm2_global_init (0);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2_global_init %d\n", rc);
      abort ();
    }

  rc = ntlm2_client_init (&ctx, NTLM2_NO_TYPE2_VALIDATION);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2_client_init %d\n", rc);
      abort ();
    }
}

void
libntlm2_done (void)
{
  ntlm2_client_done (ctx);
  ntlm2_global_done ();
}

void
libntlm2_type1 (const char *domain, const char *workstation,
		char **out, size_t * len)
{
  int rc;

  rc = ntlm2c_domain (ctx, domain);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_domain %d\n", rc);
      abort ();
    }

  rc = ntlm2c_workstation (ctx, workstation);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_workstation %d\n", rc);
      abort ();
    }

  rc = ntlm2c_type1 (ctx, out, len);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type1 %d\n", rc);
      abort ();
    }
}

void
libntlm2_type3 (const char *type2, size_t type2len,
		const char *username, const char *password,
		char **out, size_t * len)
{
  int rc;

  rc = ntlm2c_type2 (ctx, type2, type2len);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type2 failed %d\n", rc);
      abort ();
    }

  rc = ntlm2c_username (ctx, username);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_username failed %d\n", rc);
      abort ();
    }

  /* Libntlm uses the username as workstation name in Type-3. */
  rc = ntlm2c_workstation (ctx, username);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_workstation failed %d\n", rc);
      abort ();
    }

  /* Libntlm hard-code target to "mydomain". */
  rc = ntlm2c_target (ctx, "mydomain");
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_target failed %d\n", rc);
      abort ();
    }

  rc = ntlm2c_type3_passwd (ctx, password, out, len);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type3_passwd failed %d\n", rc);
      abort ();
    }
}

/* Test driver below. */

static void
hexdump (const char *name, const char *d, size_t l)
{
  size_t i;

  printf ("%s (%3.d bytes): ", name, l);
  for (i = 0; i < l; i++)
    {
      printf ("%02x", d[i] & 0xFF);
      if ((i + 1) % 16 == 0)
	{
	  size_t j;
	  printf ("\n");
	  for (j = 0; j < strlen (name) + 14; j++)
	    printf (" ");
	}
    }
  printf ("\n");
}

static const struct {
  char *domain;
  char *ws;
  char *user;
  char *passwd;
  size_t etype1len;
  char *etype1;
  size_t type2len;
  char *type2;
  size_t etype3len;
  char *etype3;
} tv[] = {
  {
    "domain",
    "ws",
    "user",
    "passwd",
    40,
    "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
    "\x01\x00\x00\x00\x07\xb2\x00\x00"
    "\x06\x00\x06\x00\x20\x00\x00\x00"
    "\x02\x00\x02\x00\x26\x00\x00\x00"
    "\x64\x6f\x6d\x61\x69\x6e\x77\x73",
    64,
    "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
    "\x02\x00\x00\x00\x10\x00\x10\x00"
    "\x30\x00\x00\x00\x00\x00\x00\x00"
    "\x01\x02\x03\x04\xf5\xc3\xb2\x82"
    "\x00\x00\x00\x00\x00\x00\x00\x00"
    "\x00\x00\x00\x00\x00\x00\x00\x00"
    "\x6d\x00\x79\x00\x64\x00\x6f\x00"
    "\x6d\x00\x61\x00\x69\x00\x6e\x00",
    144,
    "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
    "\x03\x00\x00\x00\x18\x00\x18\x00"
    "\x60\x00\x00\x00\x18\x00\x18\x00"
    "\x78\x00\x00\x00\x10\x00\x10\x00"
    "\x40\x00\x00\x00\x08\x00\x08\x00"
    "\x50\x00\x00\x00\x08\x00\x08\x00"
    "\x58\x00\x00\x00\x00\x00\x00\x00"
    "\x90\x00\x00\x00\x00\x00\x00\x00"
    "\x6d\x00\x79\x00\x64\x00\x6f\x00"
    "\x6d\x00\x61\x00\x69\x00\x6e\x00"
    "\x75\x00\x73\x00\x65\x00\x72\x00"
    "\x75\x00\x73\x00\x65\x00\x72\x00"
    "\x11\x82\x1c\x30\x99\x0a\x0c\x2f"
    "\x9b\x1c\x78\xfa\xa0\xdc\x6e\xd7"
    "\x58\x4c\xeb\x5e\x69\x8b\x79\x42"
    "\x1c\x77\x55\xd2\x45\xc8\x73\x05"
    "\x30\x39\xf8\x80\x86\xa3\x88\x43"
    "\x41\x98\x6b\x27\xbc\x44\xa5\xd5",
  },
  {
    "DOMAIN",
    "WORKSTATION",
    "user",
    "SecREt01",
    49,
    "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
    "\x01\x00\x00\x00\x07\xb2\x00\x00"
    "\x06\x00\x06\x00\x20\x00\x00\x00"
    "\x0b\x00\x0b\x00\x26\x00\x00\x00"
    "\x44\x4f\x4d\x41\x49\x4e\x57\x4f"
    "\x52\x4b\x53\x54\x41\x54\x49\x4f"
    "\x4e",
    64,
    "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
    "\x02\x00\x00\x00\x10\x00\x10\x00"
    "\x30\x00\x00\x00\x00\x00\x00\x00"
    "\x01\x02\x03\x04\xf5\xc3\xb2\x82"
    "\x00\x00\x00\x00\x00\x00\x00\x00"
    "\x00\x00\x00\x00\x00\x00\x00\x00"
    "\x6d\x00\x79\x00\x64\x00\x6f\x00"
    "\x6d\x00\x61\x00\x69\x00\x6e\x00",
    144,
    "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
    "\x03\x00\x00\x00\x18\x00\x18\x00"
    "\x60\x00\x00\x00\x18\x00\x18\x00"
    "\x78\x00\x00\x00\x10\x00\x10\x00"
    "\x40\x00\x00\x00\x08\x00\x08\x00"
    "\x50\x00\x00\x00\x08\x00\x08\x00"
    "\x58\x00\x00\x00\x00\x00\x00\x00"
    "\x90\x00\x00\x00\x00\x00\x00\x00"
    "\x6d\x00\x79\x00\x64\x00\x6f\x00"
    "\x6d\x00\x61\x00\x69\x00\x6e\x00"
    "\x75\x00\x73\x00\x65\x00\x72\x00"
    "\x75\x00\x73\x00\x65\x00\x72\x00"
    "\x6e\x76\x64\xa4\xa1\x12\x73\x82"
    "\x64\x57\x34\x58\xfd\x3f\x6d\x5c"
    "\x8d\x58\x10\xfd\x26\xf8\x79\xc5"
    "\xfa\xcd\xa8\x14\xfe\x60\xb9\x29"
    "\x4c\x57\x04\x21\x81\x3d\xb1\x89"
    "\xde\x88\x37\xbe\x8d\x73\xdb\x65",
  }
};

int
main (int argc, char *argv[])
{
  int debug = argc > 1;
  char *tmp1, *tmp2;
  size_t tmp1len, tmp2len;
  size_t i;

  if (debug)
    printf ("Libntlm library version %s header version %s\n",
	    ntlm_check_version (NULL), NTLM_VERSION);

  for (i = 0; i < sizeof (tv) / sizeof (tv[0]); i++)
    {
      if (debug)
	printf ("Iteration %d...\n", i);

      libntlm_init ();
      libntlm2_init ();

      libntlm_type1 (tv[i].domain, tv[i].ws, &tmp1, &tmp1len);
      libntlm2_type1 (tv[i].domain, tv[i].ws, &tmp2, &tmp2len);

      if (debug)
	{
	  hexdump (" Libntlm Type-1", tmp1, tmp1len);
	  hexdump ("Libntlm2 Type-1", tmp2, tmp2len);
	}

      if (tmp1len != tmp2len || tmp1len != tv[i].etype1len)
	{
	  printf ("length mismatch %d != %d != %d\n", tmp1len, tmp2len,
		  tv[i].etype1len);
	  abort ();
	}

      if (memcmp (tmp1, tv[i].etype1, tmp1len) != 0)
	{
	  printf ("Libntlm Type-1 mismatch!\n");
	  abort ();
	}

      if (memcmp (tmp2, tv[i].etype1, tmp2len) != 0)
	{
	  printf ("Libntlm2 Type-1 mismatch!\n");
	  abort ();
	}

      if (debug)
	{
	  printf ("Type-1 PASSED\n");
	  hexdump ("         Type-2", tv[i].type2, tv[i].type2len);
	}

      libntlm_type3 (tv[i].type2, tv[i].type2len,
		     tv[i].user, tv[i].passwd, &tmp1, &tmp1len);
      libntlm2_type3 (tv[i].type2, tv[i].type2len,
		      tv[i].user, tv[i].passwd, &tmp2, &tmp2len);

      if (debug)
	{
	  hexdump (" Libntlm Type-3", tmp1, tmp1len);
	  hexdump ("Libntlm2 Type-3", tmp2, tmp2len);
	}

      if (tmp1len != tmp2len || tmp1len != tv[i].etype3len)
	{
	  printf ("Length mismatch %d != %d != %d\n", tmp1len, tmp2len,
		  sizeof (tv[i].etype3));
	  abort ();
	}

      if (memcmp (tmp1, tv[i].etype3, tmp1len) != 0)
	{
	  printf ("Libntlm Type-3 mismatch\n");
	  abort ();
	}

      if (memcmp (tmp2, tv[i].etype3, tmp2len) != 0)
	{
	  printf ("Libntlm2 Type-3 mismatch\n");
	  abort ();
	}

      libntlm_done ();
      libntlm2_done ();
    }

  return 0;
}
