/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include <ntlm2.h>

#include <stdio.h>		/* printf */
#include <string.h>		/* memcmp */

/* This file demonstrates how Libntlm2 is typically used in a client
   to authenticate it to a server. */

static const char type2[] =
  /* NTLMSSP signature */
  "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
  /* Type-2 */
  "\x02\x00\x00\x00"
  /* Target name */
  "\x0c\x00\x0c\x00\x30\x00\x00\x00"
  /* Flags */
  "\x01\x02\x81\x00"
  /* Challenge */
  "\x01\x23\x45\x67\x89\xab\xcd\xef"
  /* Context */
  "\x00\x00\x00\x00\x00\x00\x00\x00"
  /* Target Information */
  "\x62\x00\x62\x00\x3c\x00\x00\x00"
  /* Target Name */
  "\x44\x00\x4f\x00\x4d\x00\x41\x00\x49\x00\x4e\x00"
  /* Target Information Data */
  "\x02\x00\x0c\x00\x44\x00\x4f\x00\x4d\x00\x41\x00\x49\x00\x4e\x00"
  "\x01\x00\x0c\x00\x53\x00\x45\x00\x52\x00\x56\x00\x45\x00\x52\x00"
  "\x04\x00\x14\x00\x64\x00\x6f\x00\x6d\x00\x61\x00\x69\x00\x6e\x00\x2e\x00\x63\x00\x6f\x00\x6d\x00"
  "\x03\x00\x22\x00\x73\x00\x65\x00\x72\x00\x76\x00\x65\x00\x72\x00\x2e\x00\x64\x00\x6f\x00\x6d\x00\x61\x00\x69\x00\x6e\x00\x2e\x00\x63\x00\x6f\x00\x6d\x00"
  "\x00\x00\x00\x00";

int
main (void)
{
  int rc;
  ntlm2c_t auth;
  char *out;
  size_t len;
  size_t i;

  /* Initialize the library. */

  rc = ntlm2_global_init (0);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2_global_init failed %d\n", rc);
      return 1;
    }

  /* Create a client context. */

  rc = ntlm2_client_init (&auth, 0);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2_client_init failed %d\n", rc);
      return 1;
    }

  /* Set domain of client. */

  rc = ntlm2c_domain (auth, "DOMAIN");
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_domain failed %d\n", rc);
      return 1;
    }

  /* Set workstation of client. */

  rc = ntlm2c_workstation (auth, "WORKSTATION");
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_workstation failed %d\n", rc);
      return 1;
    }

  /* Generate Type-1 message. */

  rc = ntlm2c_type1 (auth, &out, &len);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type1 failed %d\n", rc);
      return 1;
    }

  /* Print Type-1 message.  Normally it is sent from the client to
     server. */

  printf ("type1: ");
  for (i = 0; i < len; i++)
    printf ("%02x ", out[i] & 0xFF);
  printf ("\n");

  /* Normally you receive the Type-2 message from the server, in
     response to the Type-1 message.  We use a static copy here for
     demonstration purposes.  */

  rc = ntlm2c_type2 (auth, type2, sizeof (type2));
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type2 failed %d\n", rc);
      return 1;
    }

  /* Set username of client. */

  rc = ntlm2c_username (auth, "user");
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_username failed %d\n", rc);
      return 1;
    }

  /* Generate Type-3 message using indicated password. */

  rc = ntlm2c_type3_passwd (auth, "SecREt01", &out, &len);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type3_passwd failed %d\n", rc);
      return 1;
    }

  /* Print Type-1 message.  Normally it is sent from the client to
     server, to finish the authentication. */

  printf ("type3: ");
  for (i = 0; i < len; i++)
    printf ("%02x ", out[i] & 0xFF);
  printf ("\n");

  /* Deallocate the client context. */

  ntlm2_client_done (auth);

  /* Destroy the library when no further use is needed. */

  ntlm2_global_done ();

  return 0;
}
