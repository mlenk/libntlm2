<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
          "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd" [
	  <!ENTITY % local.common.attrib "xmlns:xi  CDATA  #FIXED 'http://www.w3.org/2003/XInclude'">
	  <!ENTITY version SYSTEM "version.xml">
	  ]>
<book id="index">
  <bookinfo>
    <title>Libntlm2 API Reference Manual</title>
    <releaseinfo>
      for Libntlm2 &version;
      The latest version of this documentation can be found on-line at
      <ulink role="online-location" url="http://josefsson.org/libntlm2/index.html">http://josefsson.org/libntlm2/</ulink>.
    </releaseinfo>
  </bookinfo>

  <chapter>
    <title>NTLM Authentication Library</title>

    <para>
      Libntlm2 is an implementation of the NTLM authentication
      protocol released under a dual BSD/LGPL license (see COPYING).
      It was written from scratch by Simon Josefsson following this
      documentation: <ulink url="http://davenport.sourceforge.net/ntlm.html">
      http://davenport.sourceforge.net/ntlm.html</ulink>.  Testing
      made sure that it is fully backwards compatible with Libntlm.
    </para>

    <para>
      WARNING!  Only use NTLM for interoperability purposes.  The
      protocol provides weak security and is based on old
      cryptographical techniques.  It is only marginally better than
      sending your password in the clear.
    </para>

    <para>
      Libntlm2 does not support all variants of NTLM.  In fact,
      Libntlm2 only sends LM+NTLM responses.  The LM response is weak,
      using only DES.  The NTLM responses is stronger but still weak
      (MD4+DES).  Note that both LM and NTLM responses are sent, thus
      the combined protocol is only as secure as LM (i.e., not at
      all).
    </para>

    <para>
      Libntlm2 does not support NTLM2 session responses nor NTLMv2.
      Libntlm2 does not support sending only the NTLM response, to
      avoid the weaknesses in the LM response.  Supporting more
      variants would be fairly straight-forward to add if someone is
      interested.
    </para>

    <para>
      Paid support and custom development of Libntlm2 is available
      through the authors' company, Simon Josefsson Datakonsult AB, a
      Stockholm based privately held company.  For more information
      see: <ulink url="http://josefsson.org/">http://josefsson.org/</ulink>
    </para>

    <xi:include href="xml/ntlm2.xml"/>

  </chapter>
  <index id="api-index-full">
    <title>API Index</title>
    <xi:include href="xml/api-index-full.xml"><xi:fallback /></xi:include>
  </index>
</book>
